﻿using System;

namespace XamFirebase.Authentication
{
    public class FirebaseAuthEventArgs : EventArgs
    {
        public readonly FirebaseAuth FirebaseAuth;

        public FirebaseAuthEventArgs(FirebaseAuth auth)
        {
            FirebaseAuth = auth;
        }
    }
}
