﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XamFirebase.Authentication;
using XamFirebase.Storage;

namespace Firebase.Storage.XamFirebase.Example
{
    internal class Program
    {
        private const string ApiKey = "your_api_key";
        private const string Bucket = "your_bucket.appspot.com";
        private const string AuthEmail = "your_email@your_email.com";
        private const string AuthPassword = "your_password";

        private static void Main()
        {
            Run().Wait();
        }

        private static async Task Run()
        {
            // Arquivo para subir
            var stream = new MemoryStream(Encoding.ASCII.GetBytes("Hello world!"));

            // Instância a autenticação
            var auth = new FirebaseAuthProvider(new FirebaseConfig(ApiKey));

            // Autentica
            var a = await auth.SignInWithEmailAndPasswordAsync(AuthEmail, AuthPassword);

            // Objeto que pode cancelar a opção
            var cancellation = new CancellationTokenSource();

            var task = new FirebaseStorage(
                Bucket,
                new FirebaseStorageOptions
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                    ThrowOnCancel = true
                })
                .Child("fotos") // Pasta
                .Child("teste.txt") // Arquivo Final
                .PutAsync(stream, cancellation.Token);

            task.Progress.ProgressChanged += (s, e) => Console.WriteLine($"Progress: {e.Percentage} %");

            // Cancelar o upload
            // cancellation.Cancel();
            try
            {
                Console.WriteLine("Download link:\n" + await task);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception was thrown: {0}", ex);
            }
        }
    }
}